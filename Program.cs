﻿using lib_FeedMeteo_Weatherstack;
using System;
using System.Collections.Generic;
using ScairimCommons;
using System.Text;
using System.IO;
using System.Threading;

namespace FeedMeteo_Weatherstack
{
    class Program
    {
        static Configuration configuration;
        static ScalaManager scalaManager;

        static void Main()
        {
            string weatherRes = "";
            string req = "";

            try
            {
                configuration = new Configuration();

                try
                {
                    Log("-");
                    Log("Init conf completed.");

                    scalaManager = Utils.initScalaManager();

                    Log("Init ScalaManager completed.");

                    Log("Start process");

                    List<string> picklistValues = scalaManager.GetPickListValues(configuration.metadataId);

                    Dictionary<string, List<string>> dicPicklistValues = Utils.GetDicPicklistValues(picklistValues);

                    Log("Created Picklist Dictionary.");

                    List<string> toSend = new List<string>();

                    foreach (string country in dicPicklistValues.Keys)
                    {
                        Log($"Creating weather_{country.ToLower()}.json");
                        //string country = "UK";
                        StringBuilder sb = new StringBuilder();
                        foreach (string city in dicPicklistValues[country])
                        {
                            sb.Append($"{city},{country};");
                        }
                        string Params = $"forecast_days={configuration.ParamForecastDays}&hourly={configuration.ParamForecastHourly}";
                        req = $"{configuration.apiEndpoint}?{configuration.apiToken}&query={sb.ToString().Replace(" ", "%20")}&{Params}";
                        //Log(req);
                        weatherRes = Http.Send("", req, "GET", "text");

                        if (weatherRes.Contains("invalid_access_key"))
                        {
                            for (int i = 0; i < 3; i++)
                            {
                                Log($"WARNING - invalid_access_key found. Retry number: {i + 1}");
                                Thread.Sleep(3000);
                                weatherRes = Http.Send("", req, "GET", "text");
                                if (!weatherRes.Contains("invalid_access_key")) break;
                            }
                        }

                        if (weatherRes.Contains("invalid_access_key"))
                        {
                            Log("ERROR - invalid_access_key. Retry failed.");
                            continue;
                        }

                        string jsonOutput = Utils.GetJsonOutput(weatherRes, configuration, dicPicklistValues[country].Count == 1, configuration.logManager);
                        string fileOutput = $"{configuration.outputPath}weather_{country.ToLower()}.json";

                        File.WriteAllText(fileOutput, jsonOutput);

                        toSend.Add(fileOutput);

                        Log("Done");
                    }

                    Log("Sending files via SFTP...");

                    Utils.SendSFTP(configuration, toSend);

                    Log("Done");

                    Log("End process");
                }
                catch (Exception e)
                {
                    Log($"{e.Message} - {e.StackTrace}");
                    Log($"Debug infos:");
                    Log($"req: {req}");
                    Log($"weatherRes: {weatherRes}");
                    Log("Failed process!!!");
                }                
            }
            catch (Exception e)
            {
                Console.WriteLine($"{e.Message} - {e.StackTrace}");
            }            
        }

        private static void Log(string text)
        {
            Console.WriteLine(text);
            configuration.logManager.Log(text);
        }
    }
}
