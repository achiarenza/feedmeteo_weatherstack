﻿using lib_FeedMeteo_Weatherstack;
using System;
using System.Collections.Generic;
using System.Text;

namespace FeedMeteo_Weatherstack
{
    public class Configuration
    {
        public LogManager logManager { get; set; }
        public string outputPath { get; set; }
        public string metadataId { get; set; }
        public string apiEndpoint { get; set; }
        public string apiToken { get; set; }
        public string SFTP_host { get; set; }
        public string SFTP_username { get; set; }
        public string SFTP_password { get; set; }
        public string SFTP_folder { get; set; }

        public int ParamForecastDays { get; set; }
        public int ParamForecastHourly { get; set; }

        public Configuration()
        {
            Utils.initApp(this);
        }
    }
}
