﻿using ScairimScala;
using System;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Text;

namespace lib_FeedMeteo_Weatherstack
{
    public class ScalaManager
    {
		private string username { get; set; }
        private string password { get; set; }
        private string urlCM { get; set; }
        private Scala scala { get; set; }
        private string apiToken { get; set; }
        private string Token { get; set; }

        public ScalaManager(string username, string password, string urlCM)
        {
            scala = new Scala();
			this.username = username;
			this.password = password;
			this.urlCM = urlCM;
			GestioneLogin();
		}

        public void GestioneLogin()
        {
			if (Token != null && Token.Length > 0)
			{
				if ((string)scala.GetSession(Token, apiToken, urlCM) != "401")
				{
					return;
				}
			}
			
			string res = (string)scala.GetLogin(username, password, urlCM);

			if (res.Length == 0)
			{
				throw new Exception("Scala CM not responding.");
			}

			JObject rJson = JObject.Parse(res);
			Token = (string)rJson["token"];
			apiToken = (string)rJson["apiToken"];
		}

		public List<string> GetPickListValues(string metadataId)
		{
			List<string> ls_res = new List<string>();

			GestioneLogin();

			string res = (string)scala.GetStuffs(Token, apiToken, $"{urlCM}/api/rest/playerMetadata/{metadataId}/pickListValues");
			if (!res.Contains("list"))
			{
				throw new Exception($"Error retriving picklist values: {res}" );
			}
			JObject rJson = JObject.Parse(res);
			foreach (JToken jToken in rJson["list"])
			{
				ls_res.Add((string)jToken["value"]);
			}

			return ls_res;
		}

		public void GetLogout()
		{
			scala.GetLogout(Token, apiToken, urlCM);
		}
	}
}
