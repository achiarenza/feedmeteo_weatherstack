﻿using System;
using System.IO;

namespace lib_FeedMeteo_Weatherstack
{
    public class LogManager
    {
        public string LogPath { get; set; }
        public int Retention { get; set; }

        public LogManager(string LogPath, int Retention)
        {
            this.LogPath = LogPath;
            this.Retention = Retention;
        }

        public void Log(string text)
        {
            string textLine;
            if (text == "-")
            {
                textLine = "-----------------------------------------------------------";
            }
            else if (text == "")
            {
                textLine = Environment.NewLine;
            }
            else
            {
                textLine = $"[{DateTime.Now}] - {text}";
            }

            using StreamWriter wr = GetStreamWriter();
            wr.WriteLine(textLine);
            wr.Close();
        }

        public void FileRetention()
        {
            foreach (string file in Directory.GetFiles(LogPath))
            {
                FileInfo f = new FileInfo(file);
                if (f.LastWriteTime < DateTime.Now.AddMonths(-Retention))
                {
                    f.Delete();
                }
            }
        }

        public StreamWriter GetStreamWriter()
        {
            string file = $"{LogPath}{System.Reflection.Assembly.GetExecutingAssembly().GetName().Name}_{DateTime.Today.ToShortDateString().Replace("/", "-")}.txt";

            if (File.Exists(file))
            {
                return File.AppendText(file);
            }
            else
            {
                FileRetention();
                return File.CreateText(file);
            }
        }
    }
}
