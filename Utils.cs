﻿using lib_FeedMeteo_Weatherstack;
using Renci.SshNet;
using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Newtonsoft.Json.Linq;

namespace FeedMeteo_Weatherstack
{
    public static class Utils
    {
        public static void initApp(Configuration conf)
        {
            conf.logManager = initLog();
            conf.outputPath = initOutput();
            conf.apiEndpoint = ConfigurationManager.AppSettings["apiEndpoint"];
            conf.apiToken = ConfigurationManager.AppSettings["apiToken"];
            conf.ParamForecastDays = int.Parse(ConfigurationManager.AppSettings["ParamForecastDays"]);
            conf.ParamForecastHourly = int.Parse(ConfigurationManager.AppSettings["ParamForecastHourly"]);
            conf.metadataId = ConfigurationManager.AppSettings["MetadataIdWeatherCity"];
            conf.SFTP_host = ConfigurationManager.AppSettings["SFTP_host"];
            conf.SFTP_username = ConfigurationManager.AppSettings["SFTP_username"];
            conf.SFTP_password = ConfigurationManager.AppSettings["SFTP_password"];
            conf.SFTP_folder = ConfigurationManager.AppSettings["SFTP_folder"];
        }

        public static ScalaManager initScalaManager()
        {
            return new ScalaManager(ConfigurationManager.AppSettings["CmUser"], 
                ConfigurationManager.AppSettings["CmPassword"], 
                ConfigurationManager.AppSettings["CmUrl"]);
        }

        public static LogManager initLog()
        {
            if (ConfigurationManager.AppSettings["LogPath"] == "")
            {
                string defLog = $"{AppDomain.CurrentDomain.BaseDirectory}logs\\";
                if (!Directory.Exists(defLog))
                {
                    Directory.CreateDirectory(defLog);
                }
                return new LogManager(defLog, int.Parse(ConfigurationManager.AppSettings["FileRetentionMonths"]));
            }
            else
            {
                return new LogManager(ConfigurationManager.AppSettings["LogPath"], int.Parse(ConfigurationManager.AppSettings["FileRetentionMonths"]));
            }
        }

        public static string initOutput()
        {
            if (ConfigurationManager.AppSettings["outputPath"] == "")
            {
                string defOutput = $"{AppDomain.CurrentDomain.BaseDirectory}output\\";
                if (!Directory.Exists(defOutput))
                {
                    Directory.CreateDirectory(defOutput);
                }
                return defOutput;
            }
            else
            {
                return ConfigurationManager.AppSettings["outputPath"];
            }
        }

        public static Dictionary<string, List<string>> GetDicPicklistValues(List<string> PicklistValues)
        {
            Dictionary<string, List<string>> res = new Dictionary<string, List<string>>();

            foreach (string value in PicklistValues)
            {
                if (!res.ContainsKey(value.Split(",")[1]))
                {
                    res.Add(value.Split(",")[1], new List<string>());
                }
                res[value.Split(",")[1]].Add(value.Split(",")[0]);
            }

            return res;
        }

        public static string GetJsonOutput(string OriginalJson, Configuration conf, bool singleCity, LogManager log)
        {
            JArray aJson;
            
            if (singleCity)
            {
                aJson = new JArray(JObject.Parse(OriginalJson));
            }
            else
            {
                aJson = JArray.Parse(OriginalJson);
            }
            JObject wJson = new JObject();
            JArray wjArray = new JArray();

            List<string> forecastDays = new List<string>();
            for (int x = 0; x < conf.ParamForecastDays; x++)
            {
                forecastDays.Add(FormatDateJson(DateTime.Now, x));
            }

            foreach (JToken cityToken in aJson)
            {
                JObject cityObject = new JObject(new JProperty("city", cityToken["location"]["name"]));

                if (cityToken["forecast"] == null)
                {
                    log.Log($"No Forecast info for {cityToken["request"]["query"]}.");
                    continue;
                }

                foreach (string day in forecastDays)
                {
                    if (cityToken["forecast"][day] == null)
                    {
                        continue;
                    }
                    JArray jHourlyArray = new JArray();
                    foreach (JToken hourToken in cityToken["forecast"][day]["hourly"])
                    {
                        jHourlyArray.Add(new JObject(
                            new JProperty("time", hourToken["time"]),
                            new JProperty("weather_code", hourToken["weather_code"]),
                            new JProperty("temperature", hourToken["temperature"])));
                    }                    
                    cityObject.Add(day, jHourlyArray);
                }
                
                wjArray.Add(cityObject);
            }

            wJson.Add("forecast", wjArray);
            
            return wJson.ToString();
        }

        public static void SendSFTP(Configuration conf, List<string> fileNames)
        {
            var connectionInfo = new ConnectionInfo(conf.SFTP_host, conf.SFTP_username, new PasswordAuthenticationMethod(conf.SFTP_username, conf.SFTP_password));
            using var sftp = new SftpClient(connectionInfo);

            sftp.Connect();
            //sftp.ChangeDirectory(conf.SFTP_folder);
            foreach (string file in fileNames)
            {
                using (var uplfileStream = File.OpenRead(file))
                {
                    sftp.UploadFile(uplfileStream, $"{conf.SFTP_folder}{Path.GetFileName(file)}", true);
                }
            }            
            sftp.Disconnect();
        }

        public static string FormatDateJson(DateTime date, int toAdd)
        {
            return date.AddDays(toAdd).ToString("yyyy-MM-dd");
        }
    }
}
